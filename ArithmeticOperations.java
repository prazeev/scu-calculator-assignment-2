import java.util.EmptyStackException;
import java.util.Stack;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.math.BigDecimal;

public class ArithmeticOperations {
    public static String result(String postfixArray[], int postfixArrayCount) // returning the final result
    {
        Stack <String> resultStack = new Stack <String> (); // the stack for the operands

        for (int i = 0; i < postfixArrayCount; i++) { // looping through the postFix array
            if (postfixArray[i] != null) {
                // if the element is a NUMBER, push it into the stack
                if (isNumber(postfixArray[i]))
                    resultStack.push(postfixArray[i]);

                // if the element is an OPERATOR
                else if (isOperator(postfixArray[i])) {
                    double first = Double.parseDouble(resultStack.pop());
                    String operator = postfixArray[i];
                    double second = 0;
                    boolean firstFact = true;
                    if(resultStack.size() > 0) {
                        second = Double.parseDouble(resultStack.pop());
                        firstFact = false;
                    }
                    double calculation = calculateOperation(operator, first, second, firstFact);
                    if(postfixArray[i] == "!" && !firstFact && postfixArray[i - 1] != "!") {
                        calculation = calculateOperation(postfixArray[i - 1], first, calculation, false);
                    }
                    // we push the result back to the stack (other operations may be done)
                    resultStack.push(calculation + ""); // adding the "" because calculation is a double and we need to cast to STRING (as the array)
                }
            }
        }
        // if there are no more operators, we pop the number as the result
        return resultStack.pop();
    } // end
    public static double calculateOperation(String operator,double first,double second, boolean firstFact) {
        double calculation = 0;
        BigDecimal opA = BigDecimal.valueOf(second);
        BigDecimal opB = BigDecimal.valueOf(first);
        switch (operator) {
            case "+":
                calculation = opA.add(opB).doubleValue();
                break;
            case "-":
                calculation = opA.subtract(opB).doubleValue();
                break;
            case "*":
                calculation = opA.multiply(opB).doubleValue();
                break;
            case "/":
                calculation = opA.divide(opB).doubleValue();
                break;
            case "!":
                if(firstFact) {
                   calculation = factorial(first); 
                } else {
                   calculation = factorial(second);
                }
                break;
        }
        return calculation;
    }
    // Calculate 
    public static double factorial(double n){    
      if (n == 0)    
        return 1;    
      else    
        return(n * factorial(n-1));    
     }
    // check if the STRING is a OPERAND
    public static boolean isNumber(String checkNumber) {
        try {
            Double.parseDouble(checkNumber);
            return true;
        } catch (Exception exception) {
            return false;
        }
    } // end
    //check if the STRING IS A OPERATOR
    public static boolean isOperator(String checkOperator) {
        switch (checkOperator) {
            case "+":
                return true;
            case "-":
                return true;
            case "*":
                return true;
            case "/":
                return true;
            case "!":
                return true;
            default:
                return false;
        }
    } // end
    // Operatiors Priority
    public static int thePriority(String operator) {
        int priority = 0; // initiate
        if (operator.equals("(") || operator.equals(")"))
            priority = 1; // last priority
        if (operator.equals("+") || operator.equals("-"))
            priority = 2; // third priority
        if (operator.equals("*") || operator.equals("/"))
            priority = 3; // second priority
        if (operator.equals("!"))
            priority = 4; // first priority
        return priority;
    } // end
    // Postfix algorithm - from INFIX to POSTFIX
    public static int postfixStack(String postfixArray[], String infixArray[], int infixArrayCount) {
        Stack <String> postfix = new Stack <String> (); // for arithmetic symbols - operators
        int countPostFix = 0; // postfixArray count initialized 
        for (int i = 0; i <= infixArrayCount; i++) { // looping through infix array
            if (infixArray[i] == null || infixArray[i].equals(""))
                continue;
            // if ( is found push it into the OPERATORS STACK
            if (infixArray[i].equals("(")) {
                if(i != 0 && isNumber(infixArray[i-1]) && thePriority(infixArray[i-1]) == 0) {
                   postfix.push("*"); 
                }
                postfix.push("(");
            }
            // if ) is found pop all the elements and add them into POSTFIX ARRAY
            else if (infixArray[i].equals(")")) {
                try // if there are matching brackets
                { // pop elements until we found the (
                    while (!postfix.peek().equals("(")) {
                        postfixArray[countPostFix] = postfix.pop();
                        countPostFix++;
                    }
                    // when the ( is found, will be popped from the stack, without using it
                    if (postfix.peek().equals("("))
                        postfix.pop();
                } catch (EmptyStackException noMatchingBraket) // if there are no matching brackets - ERROR
                {
                    Calculator.textField.setText("Matching bracket missing");
                    JOptionPane.showMessageDialog(new JFrame(), "Open bracket missing!", "Error", JOptionPane.ERROR_MESSAGE);
                    // clear the text field when the error pops up
                    CalculatorButtonAction.clear();
                }
            }
            // if is an operator
            else if (isOperator(infixArray[i])) {
                // if the current OPERATOR has higher priority than the TOP STACK element, push the element into POSTFIX ARRAY
                if (postfix.isEmpty() || thePriority(infixArray[i]) > thePriority(postfix.peek())) {
                    countPostFix++;
                    postfix.push(infixArray[i]);
                }
                // if the current OPERATOR has less priority than the top element from the STACK, pop elements from the STACK
                // until the stack is empty OR the element has a higher priority
                else {
                    while (!postfix.isEmpty() && thePriority(infixArray[i]) <= thePriority(postfix.peek())) {
                        postfixArray[countPostFix] = postfix.pop();
                        countPostFix++;
                    }
                    // then add the element to the STACK
                    postfix.push(infixArray[i]);
                }
            }
            // if the element is a number push it into POSTFIX array
            else if (isNumber(infixArray[i])) {
                postfixArray[countPostFix] = infixArray[i];
                countPostFix++;
            }
        }
        // the rest of the operators will be popped, if there are no more elements into the INFIX array
        while (!postfix.isEmpty()) {
            postfixArray[countPostFix] = postfix.pop();
            countPostFix++;
        }

        return countPostFix;
    } // end

    public static boolean theNumberIsADouble(String result) {
        // by default we accept that the number is a double
        boolean answer = true;

        double theResult = Double.parseDouble(result);
        if (theResult % 1 == 0) // if the remainder is 0, the number is an integer
            answer = false;

        return answer;
    } // end

    public static boolean isBigDecimal(String result) {
        return result.contains("E");
    }

}