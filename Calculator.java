import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JOptionPane;

public class Calculator extends JFrame {
    protected static JTextField textField;
    private JButton button;
    private JPanel forText;
    private JPanel mainButtons;
    private JPanel calculationButtons;
    private JPanel allButtons;
    private JPanel bigPanel;
    private JPanel exitAndFact;

    protected static String buttonLabel = "";
    protected static String elements = "";

    protected static int max = 100;

    protected static String infixArray[] = new String[max];
    protected static int infixArrayCount = 0;

    protected static String postfixArray[] = new String[max];
    protected static int postfixArrayCount;

    private ButtonListener readLabel = new ButtonListener();

    protected static String MS = "";
    public Calculator() {
        super("My PROG5001 - Calculator (1.0.0)");

        forText = new JPanel();
        textField = new JTextField("", 25);
        textField.setHorizontalAlignment(JTextField.RIGHT);
        textField.setEditable(false);
        textField.setBackground(Color.white);
        textField.setFont(new Font("Arial", Font.BOLD, 12));
        forText.setLayout(new GridLayout(1, 1));
        forText.add(textField);
        forText.setPreferredSize(new Dimension(300, 100));
        mainButtons = new JPanel();
        mainButtons.setLayout(new GridLayout(4, 5, 1, 1));
        // FIRST ROW
        // 1 button
        button = new JButton("1");
        button.addActionListener(readLabel);
        button.setBackground(Color.white);
        mainButtons.add(button);
        button.setFont(button.getFont().deriveFont(20f));
        
        // 2 button
        button = new JButton("2");
        button.addActionListener(readLabel);
        button.setBackground(Color.white);
        mainButtons.add(button);
        button.setFont(button.getFont().deriveFont(20f));
        
        // 3 button
        button = new JButton("3");
        button.addActionListener(readLabel);
        button.setBackground(Color.white);
        mainButtons.add(button);
        button.setFont(button.getFont().deriveFont(20f));
        
        // + button
        button = new JButton("+");
        button.addActionListener(readLabel);
        mainButtons.add(button);
        button.setToolTipText("Addition");
        button.setFont(button.getFont().deriveFont(20f));
        // backspace
        button = new JButton("\u232b"); // adding the button after the 3 empty spaces 
        button.addActionListener(readLabel);
        mainButtons.add(button);
        button.setToolTipText("Backspace");
        button.setFont(button.getFont().deriveFont(15f));
        // END ROW 1
        
        // SECOND ROW
        // 4 button
        button = new JButton("4");
        button.addActionListener(readLabel);
        button.setBackground(Color.white);
        mainButtons.add(button);
        button.setFont(button.getFont().deriveFont(20f));
        // 5 button
        button = new JButton("5");
        button.addActionListener(readLabel);
        button.setBackground(Color.white);
        mainButtons.add(button);
        button.setFont(button.getFont().deriveFont(20f));
        // 6 button
        button = new JButton("6");
        button.addActionListener(readLabel);
        button.setBackground(Color.white);
        mainButtons.add(button);
        button.setFont(button.getFont().deriveFont(20f));
        
        // subtraction button
        button = new JButton("-");
        button.addActionListener(readLabel);
        mainButtons.add(button);
        button.setToolTipText("Subtraction");
        button.setFont(button.getFont().deriveFont(20f));
        
        // clear button
        button = new JButton("C");
        button.addActionListener(readLabel);
        mainButtons.add(button);
        button.setToolTipText("Clear");
        button.setFont(button.getFont().deriveFont(20f));
        // END ROW 2
        
        // START ROW 3
        // 7 button
        button = new JButton("7");
        button.addActionListener(readLabel);
        button.setBackground(Color.white);
        mainButtons.add(button);
        button.setFont(button.getFont().deriveFont(20f));
        // 8 button
        button = new JButton("8");
        button.addActionListener(readLabel);
        button.setBackground(Color.white);
        mainButtons.add(button);
        button.setFont(button.getFont().deriveFont(20f));
        // 9 button
        button = new JButton("9");
        button.addActionListener(readLabel);
        button.setBackground(Color.white);
        mainButtons.add(button);
        button.setFont(button.getFont().deriveFont(20f));
        
        // multiplication button
        button = new JButton("*");
        button.addActionListener(readLabel);
        mainButtons.add(button);
        button.setToolTipText("Multiplication");
        button.setFont(button.getFont().deriveFont(20f));
        
        // parentheses button
        button = new JButton("(");
        button.addActionListener(readLabel);
        button.setToolTipText("Open Parenthese");
        mainButtons.add(button);
        button.setFont(button.getFont().deriveFont(20f));
        // END ROW 3
        
        // START ROW 4
        // negative button
        button = new JButton("\u2212");
        button.addActionListener(readLabel);
        mainButtons.add(button);
        button.setToolTipText("Negative Number");
        button.setFont(button.getFont().deriveFont(20f));
        
        // 0 button
        button = new JButton("0");
        button.addActionListener(readLabel);
        mainButtons.add(button);
        button.setBackground(Color.white);
        button.setFont(button.getFont().deriveFont(20f));
        
        // decimal point button
        button = new JButton(".");
        button.addActionListener(readLabel);
        mainButtons.add(button);
        button.setToolTipText("Decimal Point");
        button.setFont(button.getFont().deriveFont(20f));
        
        // division button
        button = new JButton("/");
        button.addActionListener(readLabel);
        mainButtons.add(button);
        button.setToolTipText("Division");
        button.setFont(button.getFont().deriveFont(20f));
        
        // modular button
        button = new JButton(")");
        button.addActionListener(readLabel);
        mainButtons.add(button);
        button.setToolTipText("Close Parenthese");
        button.setFont(button.getFont().deriveFont(20f));
        // END ROW 4
        
        // CALCULATION ROW
        calculationButtons = new JPanel();
        calculationButtons.setLayout(new GridLayout(1,2,2,2));
        
        // equal button
        button = new JButton("=");
        button.addActionListener(readLabel);
        button.setBackground(Color.CYAN);
        button.setToolTipText("Equal");
        button.setFont(button.getFont().deriveFont(20f));
        calculationButtons.add(button);
        
        exitAndFact = new JPanel();
        exitAndFact.setLayout(new GridLayout(1,2,2,2));
        
        // Factorial button
        button = new JButton("!");
        button.addActionListener(readLabel);
        button.setBackground(Color.CYAN);
        button.setToolTipText("Factorial");
        button.setFont(button.getFont().deriveFont(20f));
        exitAndFact.add(button);
        
        // OFF button
        button = new JButton("OFF");
        button.addActionListener(readLabel);
        button.setBackground(Color.CYAN);
        button.setToolTipText("Exit");
        button.setFont(button.getFont().deriveFont(20f));
        exitAndFact.add(button);
        
        
        // set the dimensions to the button's panel
        mainButtons.setPreferredSize(new Dimension(300, 300));
        calculationButtons.add(exitAndFact);

        // the panel for all panels with buttons
        allButtons = new JPanel();
        allButtons.setLayout(new BorderLayout());
        allButtons.add(mainButtons, BorderLayout.NORTH);
        allButtons.add(calculationButtons, BorderLayout.SOUTH);

        // adding all panels to the main panel
        bigPanel = new JPanel();
        bigPanel.setLayout(new BorderLayout());
        bigPanel.add(forText, BorderLayout.NORTH);
        bigPanel.add(allButtons, BorderLayout.SOUTH);

        // add the big panel to the frame;
        setLayout(new FlowLayout(FlowLayout.CENTER));
        add(bigPanel);
        pack();
    }

    // LISTENERS
    class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent readLabel) {
            // getting the labels symbols from the buttons and print
            // them to the text field
            buttonLabel = readLabel.getActionCommand();
            textField.setText(textField.getText() + buttonLabel);

            // CLEAR button
            if (buttonLabel.equals("C"))
                CalculatorButtonAction.clear();

            // BACKSPACE button
            if (buttonLabel.equals("\u232b"))
                CalculatorButtonAction.backspace();

            // NUMBER buttons
            if (buttonLabel.equals("0")) // if the condition is true: the user presses a DIGIT button
                CalculatorButtonAction.operandButton();

            else if (buttonLabel.equals("1"))
                CalculatorButtonAction.operandButton();

            else if (buttonLabel.equals("2"))
                CalculatorButtonAction.operandButton();

            else if (buttonLabel.equals("3"))
                CalculatorButtonAction.operandButton();

            else if (buttonLabel.equals("4"))
                CalculatorButtonAction.operandButton();

            else if (buttonLabel.equals("5"))
                CalculatorButtonAction.operandButton();

            else if (buttonLabel.equals("6"))
                CalculatorButtonAction.operandButton();

            else if (buttonLabel.equals("7"))
                CalculatorButtonAction.operandButton();

            else if (buttonLabel.equals("8"))
                CalculatorButtonAction.operandButton();

            else if (buttonLabel.equals("9"))
                CalculatorButtonAction.operandButton();
            // DECIMAL POINT
            else if (buttonLabel.equals("."))
                CalculatorButtonAction.checkDecimalPoint();
            // NEGATIVE SIGN
            else if (buttonLabel.equals("\u2212"))
                CalculatorButtonAction.negativeSign();
            // end NUMBERS

            // OPERATORS
            // DIVISION button
            if (buttonLabel.equals("/"))
                CalculatorButtonAction.operatorButton();

            // MULTIPLICATION button
            else if (buttonLabel.equals("*"))
                CalculatorButtonAction.operatorButton();

            // MINUS button
            else if (buttonLabel.equals("-"))
                CalculatorButtonAction.operatorButton();

            // PLUS button
            else if (buttonLabel.equals("+"))
                CalculatorButtonAction.operatorButton();

            // OPEN parentheses button
            else if (buttonLabel.equals("("))
                CalculatorButtonAction.operatorButton();

            // CLOSE parentheses button
            else if (buttonLabel.equals(")"))
                CalculatorButtonAction.operatorButton();

            // EQUAL button
            else if (buttonLabel.equals("="))
                CalculatorButtonAction.equal();
                
            // FACTORIAL button
            else if (buttonLabel.equals("!"))
                CalculatorButtonAction.operatorButton();
                
            // FACTORIAL button
            else if (buttonLabel.equals("OFF"))
                CalculatorButtonAction.trunOffCalculator();
        }
    } // end LISTENTERS
}