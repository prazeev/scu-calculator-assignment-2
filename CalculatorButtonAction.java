import java.util.EmptyStackException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.util.Arrays;

public class CalculatorButtonAction {
    // Clear function for button
    public static void clear() {
        for (int i = 0; i < Calculator.postfixArray.length; i++)
            Calculator.postfixArray[i] = null;
        for (int j = 0; j < Calculator.infixArray.length; j++)
            Calculator.infixArray[j] = null;
        // reset the textfield and all the elements
        // the user can start from zero with no errors
        Calculator.textField.setText("");
        Calculator.elements = "";
        Calculator.buttonLabel = "";
        Calculator.infixArrayCount = 0;
        Calculator.postfixArrayCount = 0;
    }

    public static void trunOffCalculator() {
        System.exit(0);
    }
    // Backspace button action
    public static void backspace() {
        try { // if there are characters to be deleted it will delete the last one
            // removing the last character from the textfield and not displaying the backspace symbol
            String text = Calculator.textField.getText();
            Calculator.textField.setText(text.substring(0, text.length() - 2));
            // erasing the last character from the infix array
            Calculator.infixArray[Calculator.infixArrayCount] = Calculator.infixArray[Calculator.infixArrayCount].substring(0, Calculator.infixArray[Calculator.infixArrayCount].length() - 1);
            // replacing the string in elements with the last character deleted
            Calculator.elements = Calculator.infixArray[Calculator.infixArrayCount];
        }

        // if there are no more characters to be deleted and we get 
        // StringIndexOutOfBoundsException error, the calculator will move to previous index of the infixArray
        catch (StringIndexOutOfBoundsException errorString) {
            try { // moving to the previous index of the infix array
                Calculator.infixArrayCount--;
                // erasing the last character from the infix array
                Calculator.infixArray[Calculator.infixArrayCount] =
                    Calculator.infixArray[Calculator.infixArrayCount].substring(0, Calculator.infixArray[Calculator.infixArrayCount].length() - 1);
                // replacing the string in elements with the last character deleted
                Calculator.elements = Calculator.infixArray[Calculator.infixArrayCount];
            }

            // if the index 0 of the array has no more characters to be deleted, reset the calculator
            catch (ArrayIndexOutOfBoundsException errorArray) {
                clear();
            }
        }
        // if we need to delete an arithmetic sign
        catch (NullPointerException signError) { // moving to the previous index of the infix array
            Calculator.infixArrayCount--;
            // erasing the last character from the infix array
            Calculator.infixArray[Calculator.infixArrayCount] =
                Calculator.infixArray[Calculator.infixArrayCount].substring(0, Calculator.infixArray[Calculator.infixArrayCount].length() - 1);
        }
    } // end BACKSPACE 

    // NUMBER BUTTON + DECIMAL POINT
    public static void operandButton() {
        Calculator.elements += Calculator.buttonLabel;
        Calculator.infixArray[Calculator.infixArrayCount] = Calculator.elements;

        // the pressed DIGIT is added at the end (append) to the ELEMENTS string
        // the new String is added into an array
        // we do not increase the infixArrayCount, as on this position we might append another digit
    }

    // NEGATIVE button
    public static void negativeSign() {
        if (Calculator.elements.equals("")) // if the ELEMENTS is empty because the NEGATIVE
        { // sign can be added just at the beginning of a number
            Calculator.elements += "-";
            Calculator.infixArray[Calculator.infixArrayCount] = Calculator.elements;
        } else // if the NEGATIVE sign is added in the middle of the number, ERROR
        {
            // alert, because we cannot use the negative sign as a minus!
            JOptionPane.showMessageDialog(new JFrame(), "You can not use the NEGATIVE sign for substraction!", "Calculation Error", JOptionPane.ERROR_MESSAGE);
            // clear the text field if the error pops up
            clear();
        }
    } // end NEGATIVE SIGN

    // OPERATORS BUTTONS
    public static void operatorButton() {
        Calculator.infixArrayCount++; // Until an OPERATOR is pressed, the number(s) string is added
        // to the previous index.
        Calculator.infixArray[Calculator.infixArrayCount] = Calculator.buttonLabel;
        // we add at that index the pressed OPERAND 
        Calculator.elements = "";
        // we clear the elements, to add new number or the new operand and not be mixed
        // with previous digits or operands
        Calculator.infixArrayCount++; // we increase the position of the next available index
    } // end OPERATORS BUTTONS METHOD


    // EQUAL BUTTON
    public static void equal() {
        if (Calculator.infixArray[0] == null) { // if the user inserts a digit, will overwrite the 0
            Calculator.infixArray[0] = "0";
            Calculator.textField.setText("0" + Calculator.textField.getText());
        }

        // getting the postfixArray
        Calculator.postfixArrayCount = ArithmeticOperations.postfixStack(Calculator.postfixArray, Calculator.infixArray, Calculator.infixArrayCount);
        try { // TOTAL is the result of the calculation
            String total = ArithmeticOperations.result(Calculator.postfixArray, Calculator.postfixArrayCount);
            // checking if the numbers is a DOUBLE
            if (ArithmeticOperations.theNumberIsADouble(total)) { // if the TOTAL has the decimal digits > then 0, we display the TOTAL with the decimal points.

                // display the result on the text field 
                Calculator.textField.setText(total);
                // reset infix array -> ready for next calculation
                Calculator.infixArrayCount = 0;
                // the result will be ready to be used in another calculation on [0]
                Calculator.infixArray[Calculator.infixArrayCount] = total;

                // the division by 0
                try { // if the result is INFINITY or NAN, handle the division by 0 in the exception class
                    if (Double.parseDouble(Calculator.infixArray[0]) == Double.POSITIVE_INFINITY || Double.parseDouble(Calculator.infixArray[0]) == Double.NEGATIVE_INFINITY ||
                        Double.isNaN(Double.parseDouble(Calculator.infixArray[0])))
                        throw new Exception("Division by zero");
                } catch (Exception exception) {
                    clear();
                    Calculator.infixArray[Calculator.infixArrayCount]=null;
                    JOptionPane.showMessageDialog(new JFrame(), "The denominator cannot be 0!", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
            // BIG DECIMAL
            else if (ArithmeticOperations.isBigDecimal(total))
            { // if the TOTAL is very big and is written in scientific notation, display to the screen as a bigDecimal
                Calculator.textField.setText(total);
                Calculator.infixArrayCount = 0;
                Calculator.infixArray[Calculator.infixArrayCount] = total;
            }
            // INTEGER
            else { // if the TOTAL has a decimal point, and the decimals are all 0s, we display it as a whole number
                // getting the number before the decimal point
                total = total.substring(0, total.indexOf('.'));
                Calculator.textField.setText(total);
                Calculator.infixArrayCount = 0;
                Calculator.infixArray[Calculator.infixArrayCount] = total;
            }
        } catch (Exception exception) { // the postfixt method can not handle the input
            Calculator.textField.setText("Invalid input");
            JOptionPane.showMessageDialog(new JFrame(), "Invalid input!", "Error", JOptionPane.ERROR_MESSAGE);
            // clear the text field if the error pops up
            clear();
        }
    }

    // MULTIPLE DECIMAL POINTS
    public static void checkDecimalPoint() {
        if (Calculator.infixArray[Calculator.infixArrayCount] != null) // if the DECIMAL POINT IS NOT THE FIRST CHARACTER 
        {
            if (Calculator.infixArray[Calculator.infixArrayCount].contains(".")) { // if there is already a decimal point in the number, ignore it
                String theText = Calculator.textField.getText();
                Calculator.textField.setText(theText.substring(0, theText.length() - 1));
            } else // if there is no decimal point in the number, add it to the number
                operandButton();
        } else // if the DECIMAL point starts the number, we ignore it
        {
            String theText = Calculator.textField.getText();
            Calculator.textField.setText(theText.substring(0, theText.length() - 1));
        }
    }
}